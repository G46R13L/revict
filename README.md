# README #

Desenvolva CRUDs em PHP orientado a objeto, sem utilizar framework (ex: Laravel, CodeIgniter, etc...), para cadastrar CLIENTES e suas DÍVIDAS.


Modele o banco de dados necessário para estes cadastros.


Os códigos com as regras de negócio (PHP) não devem estar nos mesmo arquivos que os códigos dos front-ends (HTML, CSS, JS, etc).


Utilize bibliotecas prontas para ajudar na aparência do front-end, ex: bootstrap.


Utilize o git e bitbucket/github para controlar o desenvolvimento e nos enviar o código pronto.



Utilize apenas as informações acima para o desenvolvimento, em caso de dúvida, siga o caminho que parece fazer mais sentido.




Dúvida
Se tiver qualquer dúvida sobre esse teste, pode me contatar pelo whatsapp.







Agradecemos sua participação e boa sorte!