<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<?php include('include/head.php'); ?>
</head>
  <body>
    <?php include('include/topo-site.php'); ?>
    <div class="container">
      <div class="row you-are-here">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <span class="label label-default">Você está aqui</span>
          <ol class="breadcrumb mt5px">
            <li class="active">Home</li>
            <!-- <li><a href="#">Library</a></li>
            <li class="active">Data</li> -->
          </ol>
        </div>
      </div>
      <div class="row mt5px">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="alert alert-info" role="alert">
            Teste para vaga de programador Php FullStack<br>
            Candidato: Gabriel Filipe Carvalho<br>
            Data do Teste: 15/09/2018<br>
            URL Repositório do Teste(Bitbucket): https://G46R13L@bitbucket.org/G46R13L/revict.git
          </div>
        </div>
      </div>
    </div>
  </body>
</html>