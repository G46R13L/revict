<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<?php include('include/head.php'); ?>
</head>
  <body>
    <?php include('include/topo-site.php'); ?>
    <div class="container">
      <div class="row you-are-here">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <span class="label label-default">Você está aqui</span>
          <ol class="breadcrumb mt5px">
            <li><a href="index.php">Home</a></li>
            <li class="active">Listar Dívidas dos Clientes</li>
          </ol>
        </div>
      </div>
      <div class="row mt5px mb20px">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <table class="table table-hover table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Empresa</th>
                <th>Cliente</th>
                <th>Valor do Debito</th>
                <th>Dívida Ativa?</th>
                <th>Data Cadastro</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbl-dividas"></tbody>
          </table>
        </div>
      </div>
    </div>
    <script src="js/divida.js"></script>
  </body>
</html>