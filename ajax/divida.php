<?php 

require_once('../include/classes/Divida.class.php');
require_once('../include/classes/Empresa.class.php');
require_once('../include/classes/Cliente.class.php');

$opcao = addslashes($_POST['opcao']);

switch($opcao){
	case 'obter-dados-iniciais':
		$empresa = new Empresa();
		$cliente = new Cliente();
		$retorno = array();
		$retorno[] = array(
			'empresa' => $empresa->SelectboxEmpresas(),
			'cliente' => $cliente->GetSelectboxClientes(),
		);
		echo json_encode($retorno);
	break;
	case 'cadastrar-divida':
		$divida = new Divida();
		$divida->id_empresa = addslashes($_POST['empresa']);
		$divida->id_cliente = addslashes($_POST['cliente']);
		$divida->valor_divida = addslashes($_POST['valor_divida']);
		$retorno = $divida->CadastrarDivida();
		if($retorno == 'cadastrou'){
			echo 'cadastrou';
		}else if($retorno == 'nao cadastrou'){
			echo 'nao cadastrou';
		}
	break;
	case 'load-table-dividas':
		$divida = new Divida();
		$tabela = $divida->GetDividasTable();
		echo $tabela;
	break;
	case 'delete-divida':
		$divida = new Divida();
		$divida->id_divida = addslashes($_POST['id_divida']);
		$retorno = $divida->DeleteDivida();
		if($retorno == 'deletou'){
			echo 'deletou';
		}else if($retorno == 'nao deletou'){
			echo 'nao deletou';
		}
	break;
	case 'obter-dados-divida':
		$divida = new Divida();
		$divida->id_divida = addslashes($_POST['id_divida']);
		$retorno = $divida->ObterDadosDivida();
		echo json_encode($retorno);
	break;
	case 'update-divida':
		$divida = new Divida();
		$divida->id_divida = addslashes($_POST['id_divida']);
		$divida->id_empresa = addslashes($_POST['empresa']);
		$divida->id_cliente = addslashes($_POST['cliente']);
		$divida->ativo = addslashes($_POST['ativo']);
		$divida->valor_divida = addslashes($_POST['valor_divida']);
		$retorno = $divida->UpdateDivida();
		if($retorno == 'atualizou'){
			echo 'atualizou';
		}else if($retorno == 'nao atualizou'){
			echo 'nao atualizou';
		}
	break;
}

?>