<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<?php include('include/head.php'); ?>
</head>
  <body>
    <?php include('include/topo-site.php'); ?>
    <div class="container">
      <div class="row you-are-here">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <span class="label label-default">Você está aqui</span>
          <ol class="breadcrumb mt5px">
            <li><a href="index.php">Home</a></li>
            <li class="active">Editar Cadastro do Cliente</li>
          </ol>
        </div>
      </div>
      <div class="row mt5px mb20px">
        <div class="col-xs-12 col-sm-4 col-md-4"></div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <form action="javascript:void(0);" method="POST" id="cad-cliente">
            <div class="agrupa-input">
              <span class="label label-default">Nome Completo</span><br>
              <input type="text" name="nome" id="nome" placeholder="Nome" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">RG - somente números</span><br>
              <input type="text" name="rg" id="rg" placeholder="RG, somente números" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">CPF - somente números</span><br>
              <input type="text" name="cpf" id="cpf" placeholder="CPF, somente números" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Celular</span><br>
              <input type="text" name="celular" id="celular" placeholder="Celular" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">E-mail</span><br>
              <input type="email" name="email" id="email" placeholder="E-mail válido" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Endereço</span><br>
              <input type="text" name="endereco" id="endereco" placeholder="Endereço" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Número</span><br>
              <input type="text" name="numero" id="numero" placeholder="Número" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Complemento</span><br>
              <input type="text" name="complemento" id="complemento" placeholder="Complemento" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Bairro</span><br>
              <input type="text" name="bairro" id="bairro" placeholder="Bairro" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">CEP - somente números</span><br>
              <input type="number" min="0" maxlength="8" name="cep" id="cep" placeholder="CEP" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Cidade</span><br>
              <input type="text" name="cidade" id="cidade" placeholder="Cidade" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">UF</span><br>
              <select name="uf" id="uf" required="required" class="form-control">
                <option value="">Selecione uma UF</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
              </select>
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Cliente Ativo?</span><br>
              <select name="ativo" id="ativo" required="required" class="form-control">
                <option value="1">SIM</option>
                <option value="0">NÃO</option>
              </select>
            </div>
            <div class="agrupa-input mt20px">
              <input type="hidden" name="id_cliente" id="id_cliente" value="" />
              <input type="submit" name="btn-atualizar" value="Atualizar" id="btn-atualizar" class="btn btn-sm btn-success pull-right" />
            </div>
          </form>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4"></div>
      </div>
    </div>
    <script src="js/clientes.js"></script>
  </body>
</html>