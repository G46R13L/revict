<?php 

require_once('Database.class.php');

class Cliente{

	private $id_cliente;
	private $nome;
	private $rg;
	private $cpf;
	private $celular;
	private $email;
	private $endereco;
	private $numero;
	private $complemento;
	private $bairro;
	private $cep;
	private $cidade;
	private $uf;
	private $ativo;
	private $data_cadastro;

	public function __construct() {
  	
  }

  public function __set($name, $value){
  	$this->$name = $value;
 	}
 
	public function __get($name){
  	return $this->$name;
	}

  public function CadastrarCliente(){
  	try{
   		$sql = "INSERT INTO clientes(    
				nome,
				rg,
				cpf,
				celular,
				email, 
				endereco, 
				numero, 
				complemento, 
				bairro, 
				cep, 
				cidade, 
				uf 
			)VALUES(
					:nome,
					:rg,
					:cpf,
					:celular,
					:email, 
					:endereco, 
					:numero, 
					:complemento, 
					:bairro, 
					:cep, 
					:cidade, 
					:uf 
     		)
     	";
     	$p_sql = Database::getInstance()->prepare($sql);
			$p_sql->bindValue(":nome", $this->nome);
     	$p_sql->bindValue(":rg", $this->rg);
     	$p_sql->bindValue(":cpf", $this->cpf);
     	$p_sql->bindValue(":celular", $this->celular);
     	$p_sql->bindValue(":email", $this->email);
     	$p_sql->bindValue(":endereco", $this->endereco);
     	$p_sql->bindValue(":numero", $this->numero);
     	$p_sql->bindValue(":complemento", $this->complemento);
     	$p_sql->bindValue(":bairro", $this->bairro);
     	$p_sql->bindValue(":cep", $this->cep);
     	$p_sql->bindValue(":cidade", $this->cidade);
     	$p_sql->bindValue(":uf", $this->uf);
     	$retorno = $p_sql->execute();
     	if($retorno){
     		return 'cadastrou';
     	}else{
     		return 'nao cadastrou';
     	}
 		}catch(Exception $e){
			echo 'Erro ao tentar cadastrar o Cliente: '.$e->getMessage();
 		}
  }

  public function UpdateCliente(){
  	try{
   		$sql = "
   			UPDATE 
   				clientes 
 				SET 
					nome = :nome,
					rg = :rg,
					cpf = :cpf,
					celular = :celular,
					email = :email, 
					endereco = :endereco, 
					numero = :numero, 
					complemento = :complemento, 
					bairro = :bairro, 
					cep = :cep, 
					cidade = :cidade, 
					uf = :uf, 
					ativo = :ativo 
				WHERE 
					id_cliente = :id_cliente
     	";
     	$p_sql = Database::getInstance()->prepare($sql);
			$p_sql->bindValue(":nome", $this->nome);
     	$p_sql->bindValue(":rg", $this->rg);
     	$p_sql->bindValue(":cpf", $this->cpf);
     	$p_sql->bindValue(":celular", $this->celular);
     	$p_sql->bindValue(":email", $this->email);
     	$p_sql->bindValue(":endereco", $this->endereco);
     	$p_sql->bindValue(":numero", $this->numero);
     	$p_sql->bindValue(":complemento", $this->complemento);
     	$p_sql->bindValue(":bairro", $this->bairro);
     	$p_sql->bindValue(":cep", $this->cep);
     	$p_sql->bindValue(":cidade", $this->cidade);
     	$p_sql->bindValue(":uf", $this->uf);
     	$p_sql->bindValue(":ativo", $this->ativo);
     	$p_sql->bindValue(":id_cliente", $this->id_cliente);
     	$retorno = $p_sql->execute();
     	if($retorno){
     		return 'atualizou';
     	}else{
     		return 'nao atualizou';
     	}
 		}catch(Exception $e){
			echo 'Erro ao tentar atualizar o cadastro do Cliente #'.$this->id_cliente.': '.$e->getMessage();
 		}
  }

  public function GetSelectboxClientes(){
  	$sql = "
  		SELECT 
  			id_cliente, 
  			nome 
			FROM 
				clientes
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$tbl = "<option value=''>Selecione o Cliente</option>";
		foreach($result as $rs){
		  $tbl.="
		  	<option value='".$rs['id_cliente']."'>".$rs['nome']."</option>
		  ";
		}
		return $tbl;
  }

  public function GetCustomersTable(){
  	$sql = "
  		SELECT 
  			id_cliente, 
  			nome, 
  			rg,
  			cpf,
  			celular, 
  			email,
  			CASE WHEN ativo = 1 THEN 'SIM' ELSE 'NÃO' END as ativo, 
  			data_cadastro 
			FROM Clientes
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$tbl = "";
		if(empty($result)){
			$tbl = "
				<tr>
					<td colspan='10'><center style='color: red;'>NENHUM RESULTADO ENCONTRADO</center></td>
				</tr>
			";
		}
		foreach($result as $rs){
		  $tbl.="
		  	<tr>
		  		<td>".$rs['id_cliente']."</td>
		  		<td>".$rs['nome']."</td>
		  		<td>".$rs['rg']."</td>
		  		<td>".$rs['cpf']."</td>
		  		<td>".$rs['celular']."</td>
		  		<td>".$rs['email']."</td>
		  		<td>".$rs['ativo']."</td>
		  		<td>".date('d/m/Y H:i:s',strtotime($rs['data_cadastro']))."</td>
		  		<td><a href='editar-cliente.php?id_cliente=".$rs['id_cliente']."' title='editar cliente' class='btn btn-xs btn-info'>editar</a></td>
		  		<td><a href='javascript:void(0);' title='excluir cliente' class='btn btn-xs btn-danger btn-delete' data-id-cliente='".$rs['id_cliente']."'>excluir</a></td>
		  	</tr>
		  ";
		}
		return $tbl;
  }

  public function DeleteCustomer(){
  	$sql = "DELETE FROM Clientes WHERE id_cliente = :id_cliente";
   	$p_sql = Database::getInstance()->prepare($sql);
		$p_sql->bindValue(":id_cliente", $this->id_cliente);
		$retorno = $p_sql->execute();
   	if($retorno){
   		return 'deletou';
   	}else{
   		return 'nao deletou';
   	}
  }

  public function ObterDadosCliente(){
  	$sql = "
  		SELECT 
  			*
			FROM Clientes 
			WHERE 
				id_cliente = ".$this->id_cliente."
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$dados = array();
		foreach($result as $rs){
		  $dados[] = array(
		  	'id_cliente' => $rs['id_cliente'], 
		  	'nome' => $rs['nome'], 
		  	'rg' => $rs['rg'], 
		  	'cpf' => $rs['cpf'], 
		  	'celular' => $rs['celular'], 
		  	'email' => $rs['email'], 
		  	'endereco' => $rs['endereco'], 
		  	'numero' => $rs['numero'], 
		  	'complemento' => $rs['complemento'], 
		  	'bairro' => $rs['bairro'], 
		  	'cep' => $rs['cep'], 
		  	'cidade' => $rs['cidade'], 
		  	'uf' => $rs['uf'], 
		  	'ativo' => $rs['ativo'], 
		  	'data_cadastro' => $rs['data_cadastro'] 
		  );
		}
		return $dados;
  }

  public function __destruct() {
  	
  }


}


?>