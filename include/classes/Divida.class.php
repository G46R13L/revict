<?php 

require_once('Database.class.php');
require_once('Empresa.class.php');
require_once('Cliente.class.php');

class Divida{

	private $id_divida;
	private $id_cliente;
	private $id_empresa;
	private $valor_divida;
	private $ativo;
	private $data_cadastro;

	public function __construct() {
  	
  }

  public function __set($name, $value){
  	$this->$name = $value;
 	}
 
	public function __get($name){
  	return $this->$name;
	}

  public function CadastrarDivida(){
  	try{
   		$sql = "INSERT INTO divida(    
				id_empresa, 
				id_cliente, 
				valor_divida  
			)VALUES(
					:id_empresa,
					:id_cliente,
					:valor_divida
     		)
     	";
     	$source = array('.', ',');
			$replace = array('', '.');
     	$p_sql = Database::getInstance()->prepare($sql);
			$p_sql->bindValue(":id_empresa", $this->id_empresa);
     	$p_sql->bindValue(":id_cliente", $this->id_cliente);
     	$p_sql->bindValue(":valor_divida", str_replace($source, $replace, $this->valor_divida));
     	$retorno = $p_sql->execute();
     	if($retorno){
     		return 'cadastrou';
     	}else{
     		return 'nao cadastrou';
     	}
 		}catch(Exception $e){
			echo 'Erro ao tentar cadastrar a Dìvida: '.$e->getMessage();
 		}
  }

  public function UpdateDivida(){
  	try{
   		$sql = "
   			UPDATE 
   				divida 
 				SET 
					id_empresa = :id_empresa,
					id_cliente = :id_cliente,
					valor_divida = :valor_divida, 
					ativo = :ativo 
				WHERE 
					id_divida = :id_divida
     	";
     	$p_sql = Database::getInstance()->prepare($sql);
			$p_sql->bindValue(":id_empresa", $this->id_empresa);
     	$p_sql->bindValue(":id_cliente", $this->id_cliente);
     	$p_sql->bindValue(":valor_divida", $this->valor_divida);
     	$p_sql->bindValue(":ativo", $this->ativo);
     	$p_sql->bindValue(":id_divida", $this->id_divida);
     	$retorno = $p_sql->execute();
     	if($retorno){
     		return 'atualizou';
     	}else{
     		return 'nao atualizou';
     	}
 		}catch(Exception $e){
			echo 'Erro ao tentar atualizar a Dìvida #'.$this->id_divida.': '.$e->getMessage();
 		}
  }

  public function GetDividasTable(){
  	$sql = "
  		SELECT 
  			d.id_cliente, 
  			d.id_empresa, 
  			d.id_divida, 
  			d.valor_divida, 
  			CASE WHEN d.ativo = 1 THEN 'SIM' ELSE 'NÃO' END as ativo, 
  			d.data_cadastro, 
  			c.nome, 
  			e.empresa  
			FROM 
				divida d
			INNER JOIN clientes c ON c.id_cliente = d.id_cliente 
			INNER JOIN empresa e ON e.id_empresa = d.id_empresa 
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$tbl = "";
		if(empty($result)){
			$tbl = "
				<tr>
					<td colspan='8'><center style='color: red;'>NENHUM RESULTADO ENCONTRADO</center></td>
				</tr>
			";
		}
		foreach($result as $rs){
		  $tbl.="
		  	<tr>
		  		<td>".$rs['id_divida']."</td>
		  		<td>".$rs['empresa']."</td>
		  		<td>".$rs['nome']."</td>
		  		<td>R$ ".number_format($rs['valor_divida'],2,',','.')."</td>
		  		<td>".$rs['ativo']."</td>
		  		<td>".date('d/m/Y H:i:s',strtotime($rs['data_cadastro']))."</td>
		  		<td><a href='editar-divida.php?id_divida=".$rs['id_divida']."' title='editar divida' class='btn btn-xs btn-info'>editar</a></td>
		  		<td><a href='javascript:void(0);' title='excluir divida' class='btn btn-xs btn-danger btn-delete' data-id-divida='".$rs['id_divida']."'>excluir</a></td>
		  	</tr>
		  ";
		}
		return $tbl;
  }

  public function DeleteDivida(){
  	$sql = "DELETE FROM divida WHERE id_divida = :id_divida";
   	$p_sql = Database::getInstance()->prepare($sql);
		$p_sql->bindValue(":id_divida", $this->id_divida);
		$retorno = $p_sql->execute();
   	if($retorno){
   		return 'deletou';
   	}else{
   		return 'nao deletou';
   	}
  }

  public function ObterDadosDivida(){
  	$sql = "
  		SELECT 
  			d.id_cliente, 
  			d.id_empresa, 
  			d.id_divida, 
  			d.valor_divida, 
  			d.ativo as ativo2, 
  			CASE WHEN d.ativo = 1 THEN 'SIM' ELSE 'NÃO' END as ativo, 
  			d.data_cadastro, 
  			c.nome, 
  			e.empresa  
			FROM 
				divida d
			INNER JOIN clientes c ON c.id_cliente = d.id_cliente 
			INNER JOIN empresa e ON e.id_empresa = d.id_empresa 
			WHERE 
				d.id_divida = ".$this->id_divida."
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$dados = array();
		$empresa = new Empresa();
		$cliente = new Cliente();
		$company = $empresa->SelectboxEmpresas();
		$cli = $cliente->GetSelectboxClientes();
		foreach($result as $rs){
		  $dados[] = array(
		  	'id_cliente' => $rs['id_cliente'], 
		  	'id_empresa' => $rs['id_empresa'], 
		  	'valor_divida' => number_format($rs['valor_divida'],2,',','.'),  
		  	'ativo' => $rs['ativo'], 
		  	'ativo2' => $rs['ativo2'], 
		  	'data_cadastro' => $rs['data_cadastro'],
		  	'empresa' => $company, 
		  	'cliente' => $cli
		  );
		}
		return $dados;
  }

  public function __destruct() {
  	
  }


}


?>