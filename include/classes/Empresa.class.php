<?php 

require_once('Database.class.php');

class Empresa{

	private $id_empresa;
	private $empresa;
	private $ativo;
	private $data_cadastro;

	public function __construct() {
  	
  }

  public function __set($name, $value){
  	$this->$name = $value;
 	}
 
	public function __get($name){
  	return $this->$name;
	}

  public function SelectboxEmpresas(){
  	$sql = "
  		SELECT 
  			id_empresa, 
  			empresa 
			FROM 
				empresa
		";
		$select = Database::getInstance()->query($sql);
		$result = $select->fetchAll();
		$tbl = "<option value=''>Selecione uma Empresa</option>";
		foreach($result as $rs){
		  $tbl.="
		  	<option value='".$rs['id_empresa']."'>".$rs['empresa']."</option>
		  ";
		}
		return $tbl;
  }

  public function __destruct() {
  	
  }


}

?>