<div class="container">
  <div class="row topo-header">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <ul class="nav nav-pills">
        <li role="presentation"><a href="index.php">Home</a></li>
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Clientes <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="cad-cliente.php">Cadastrar Cliente</a></li>
            <li><a href="listar-clientes.php">Listar Clientes</a></li>
          </ul>
        </li>
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Dívidas <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="cad-divida.php">Cadastrar Divida</a></li>
            <li><a href="listar-dividas.php">Listar Dívidas</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>