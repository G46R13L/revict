(function ($) {
	"use strict";
	function Cliente() {
	    
	  let _cli = this;
	  
	  _cli.prps = {
	      // AJAX definitions
	      ajaxFile: 'ajax/cliente.php'
	  };

	  _cli._constructor = function() {
  		_cli.CadastrarCliente();
  		_cli.clickClearButton();
  		_cli.loadPageListarClientes();
  		_cli.DeleteCustomer();
  		_cli.UpdateCliente();
	  };


	  _cli.CadastrarCliente = function(){
	  	$(document).on('click','#btn-salvar',function(){
	  		let nome = $('#nome').val();
	  		let rg = $('#rg').val();
	  		let cpf = $('#cpf').val();
	  		let celular = $('#celular').val();
	  		let email = $('#email').val();
	  		let endereco = $('#endereco').val();
	  		let numero = $('#numero').val();
	  		let complemento = $('#complemento').val();
	  		let bairro = $('#bairro').val();
	  		let cep = $('#cep').val();
	  		let cidade = $('#cidade').val();
	  		let uf = $('#uf').val();
	  		let post = {};
	  		post['opcao'] = 'cadastrar-cliente';
	  		post['nome'] = nome;
	  		post['rg'] = rg;
	  		post['cpf'] = cpf;
	  		post['celular'] = celular;
	  		post['email'] = email;
	  		post['endereco'] = endereco;
	  		post['numero'] = numero;
	  		post['complemento'] = complemento;
	  		post['bairro'] = bairro;
	  		post['cep'] = cep;
	  		post['cidade'] = cidade;
	  		post['uf'] = uf;
	  		$.ajax({
	        type: 'POST',
	        url: _cli.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	if(data == 'cadastrou'){
	        		alert('Cliente cadastrado com sucesso');
	        		_cli.limparFormulario();
	        	}else if(data == 'nao cadastrou'){
	        		alert('Não foi possível cadastrar o Cliente');
	        		_cli.limparFormulario();
	        	}
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao cadastrar cliente: '+msg);
		    	}
        });
	  	});
	  };

	  _cli.UpdateCliente = function(){
	  	$(document).on('click','#btn-atualizar',function(){
	  		let nome = $('#nome').val();
	  		let rg = $('#rg').val();
	  		let cpf = $('#cpf').val();
	  		let celular = $('#celular').val();
	  		let email = $('#email').val();
	  		let endereco = $('#endereco').val();
	  		let numero = $('#numero').val();
	  		let complemento = $('#complemento').val();
	  		let bairro = $('#bairro').val();
	  		let cep = $('#cep').val();
	  		let cidade = $('#cidade').val();
	  		let uf = $('#uf').val();
	  		let id_cliente = $('#id_cliente').val();
	  		let ativo = $('#ativo').val();
	  		let post = {};
	  		post['opcao'] = 'update-cliente';
	  		post['nome'] = nome;
	  		post['rg'] = rg;
	  		post['cpf'] = cpf;
	  		post['celular'] = celular;
	  		post['email'] = email;
	  		post['endereco'] = endereco;
	  		post['numero'] = numero;
	  		post['complemento'] = complemento;
	  		post['bairro'] = bairro;
	  		post['cep'] = cep;
	  		post['cidade'] = cidade;
	  		post['uf'] = uf;
	  		post['id_cliente'] = id_cliente;
	  		post['ativo'] = ativo;
	  		$.ajax({
	        type: 'POST',
	        url: _cli.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	if(data == 'atualizou'){
	        		alert('Cliente atualizado com sucesso');
	        	}else if(data == 'nao atualizou'){
	        		alert('Não foi possível atualizar o cadastro desse Cliente');
	        	}
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao atualizar cadastro desse cliente: '+msg);
		    	}
        });
	  	});
	  };

	  _cli.limparFormulario = function(){
	  	$('form#cad-cliente input[type="text"], input[type="number"], input[type="email"]').val('');
	  	$('form#cad-cliente #uf').val('').prop('selected',true);
	  	$('form#cad-cliente #nome').focus();
	  };

	  _cli.clickClearButton = function(){
	  	$(document).on('click','#btn-limpar',function(){
	  		 _cli.limparFormulario();
	  	});
	  };

	  _cli.loadPageListarClientes = function(){
	  	let pagina = window.location.pathname.split('/');
	  	pagina = pagina[pagina.length - 1];
	  	if(pagina == 'listar-clientes.php'){
	  		let post = {};
	  		post['opcao'] = 'load-table-clientes';
	  		$.ajax({
	        type: 'POST',
	        url: _cli.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	$('#tbl-clientes').empty();
	        	$('#tbl-clientes').html(data);
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao listar os clientes: '+msg);
		    	}
        });
	  	}else if(pagina.indexOf('editar-cliente.php') != -1){
	  		let id_cliente = _cli.getParameterByName('id_cliente');
	  		let post = {};
	  		post['opcao'] = 'obter-dados-cliente';
	  		post['id_cliente'] = id_cliente;
	  		$.ajax({
	        type: 'POST',
	        url: _cli.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	data = JSON.parse(data);
	        	$('#nome').val(data[0].nome);
	        	$('#rg').val(data[0].rg);
	        	$('#cpf').val(data[0].cpf);
	        	$('#celular').val(data[0].celular);
	        	$('#email').val(data[0].email);
	        	$('#endereco').val(data[0].endereco);
	        	$('#numero').val(data[0].numero);
	        	$('#complemento').val(data[0].complemento);
	        	$('#bairro').val(data[0].bairro);
	        	$('#cep').val(data[0].cep);
	        	$('#cidade').val(data[0].cidade);
	        	$('#uf').val(data[0].uf).prop('selected',true);
	        	$('#ativo').val(data[0].ativo).prop('selected',true);
	        	$('#id_cliente').val(data[0].id_cliente);
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao obter os dados do cliente: '+msg);
		    	}
        });
	  	}
	  };

	  _cli.getParameterByName = function(name, url){
	  	if (!url) url = window.location.href;
	    url = url.toLowerCase(); // correcao em caso de case sensitive
	    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// correcao em caso de case sensitive
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	  };

	  _cli.DeleteCustomer = function(){
	  	$(document).on('click','a.btn-delete',function(){
	  		let id_cliente = $(this).attr('data-id-cliente');
	  		let confirma = window.confirm('Deseja realmente excluir esse Cliente?');
	  		if(confirma){
	  			let post = {};
		  		post['opcao'] = 'delete-cliente';
		  		post['id_cliente'] = id_cliente;
		  		$.ajax({
		        type: 'POST',
		        url: _cli.prps.ajaxFile,
		        data: post,
		        dataType: 'text',
		        success: function(data){
		        	if(data == 'deletou'){
		        		alert('Cliente excluído com sucesso');
		        		_cli.loadPageListarClientes();
		        	}else if(data == 'nao deletou'){
		        		alert('Não foi possível excluir o Cliente');
		        	}
		        },
	          error: function (jqXHR, exception) {
			        var msg = '';
			        if (jqXHR.status === 0) {
			            msg = 'Not connect.\n Verify Network.';
			        } else if (jqXHR.status == 404) {
			            msg = 'Requested page not found. [404]';
			        } else if (jqXHR.status == 500) {
			            msg = 'Internal Server Error [500].';
			        } else if (exception === 'parsererror') {
			            msg = 'Requested JSON parse failed.';
			        } else if (exception === 'timeout') {
			            msg = 'Time out error.';
			        } else if (exception === 'abort') {
			            msg = 'Ajax request aborted.';
			        } else {
			            msg = 'Uncaught Error.\n' + jqXHR.responseText;
			        }
			        console.error('erro ao excluir o cliente: '+msg);
			    	}
	        });
	  		}
	  	});
	  };

		this._constructor();

	}

  $(document).ready(function () {
      new Cliente();
  });

})(window.jQuery);