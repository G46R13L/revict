(function ($) {
	"use strict";
	function Divida() {
	    
	  let _divida = this;
	  
	  _divida.prps = {
	      // AJAX definitions
	      ajaxFile: 'ajax/divida.php', 
	      ajaxFileEmpresa: 'ajax/empresa.php' 
	  };

	  _divida._constructor = function() {
  		_divida.CadastrarDivida();
  		_divida.clickClearButton();
  		_divida.loadPageListarDividas();
  		_divida.DeleteDivida();
  		_divida.UpdateDivida();
	  };


	  _divida.CadastrarDivida = function(){
	  	$(document).on('click','#btn-salvar',function(){
	  		let empresa = $('#empresa').val();
	  		let cliente = $('#cliente').val();
	  		let valor_divida = $('#valor_divida').val();
	  		let post = {};
	  		post['opcao'] = 'cadastrar-divida';
	  		post['empresa'] = empresa;
	  		post['cliente'] = cliente;
	  		post['valor_divida'] = valor_divida;
	  		$.ajax({
	        type: 'POST',
	        url: _divida.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	if(data == 'cadastrou'){
	        		alert('Dívida cadastrada com sucesso');
	        		_divida.limparFormulario();
	        	}else if(data == 'nao cadastrou'){
	        		alert('Não foi possível cadastrar a Dìvida');
	        		_divida.limparFormulario();
	        	}
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao cadastrar a Dívida: '+msg);
		    	}
        });
	  	});
	  };

	  _divida.UpdateDivida = function(){
	  	$(document).on('click','#btn-atualizar',function(){
	  		let empresa = $('#empresa').val();
	  		let cliente = $('#cliente').val();
	  		let ativo = $('#ativo').val();
	  		let valor_divida = $('#valor_divida').val();
	  		valor_divida = valor_divida.replace('.','');
	  		valor_divida = valor_divida.replace(',','.');
	  		let id_divida = $('#id_divida').val();
	  		let post = {};
	  		post['opcao'] = 'update-divida';
	  		post['empresa'] = empresa;
	  		post['cliente'] = cliente;
	  		post['valor_divida'] = valor_divida;
	  		post['id_divida'] = id_divida;
	  		post['ativo'] = ativo;
	  		$.ajax({
	        type: 'POST',
	        url: _divida.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	if(data == 'atualizou'){
	        		alert('Dívida atualizada com sucesso');
	        	}else if(data == 'nao atualizou'){
	        		alert('Não foi possível atualizar a Dìvida desse Cliente');
	        	}
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao atualizar a Dìvida desse cliente: '+msg);
		    	}
        });
	  	});
	  };

	  _divida.limparFormulario = function(){
	  	$('form#cad-cliente select').val('').prop('selected',true);
	  	$('form#cad-cliente input[type="text"]').val('');
	  };

	  _divida.clickClearButton = function(){
	  	$(document).on('click','#btn-limpar',function(){
	  		 _divida.limparFormulario();
	  	});
	  };

	  _divida.loadPageListarDividas = function(){
	  	let pagina = window.location.pathname.split('/');
	  	pagina = pagina[pagina.length - 1];
	  	if(pagina == 'listar-dividas.php'){
	  		let post = {};
	  		post['opcao'] = 'load-table-dividas';
	  		$.ajax({
	        type: 'POST',
	        url: _divida.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	$('#tbl-dividas').empty();
	        	$('#tbl-dividas').html(data);
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao listar as dividas: '+msg);
		    	}
        });
	  	}else if(pagina.indexOf('editar-divida.php') != -1){
	  		let id_divida = _divida.getParameterByName('id_divida');
	  		let post = {};
	  		post['opcao'] = 'obter-dados-divida';
	  		post['id_divida'] = id_divida;
	  		$.ajax({
	        type: 'POST',
	        url: _divida.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	data = JSON.parse(data);
	        	$('#empresa').empty();
	        	$('#empresa').html(data[0].empresa).val(data[0].id_empresa).prop('selected',true);
	        	$('#cliente').empty();
	        	$('#cliente').html(data[0].cliente).val(data[0].id_cliente).prop('selected',true);
	        	//$('#empresa').val(data[0].id_empresa).prop('selected',true);
	        	//$('#cliente').val(data[0].id_cliente).prop('selected',true);
	        	$('#valor_divida').val(data[0].valor_divida);
	        	$('#id_divida').val(id_divida);
	        	$('#ativo').val(data[0].ativo2).prop('selected',true);
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao obter os dados da Divida: '+msg);
		    	}
        });
	  	}else if(pagina == 'cad-divida.php'){
	  		let post = {};
	  		post['opcao'] = 'obter-dados-iniciais';
	  		$.ajax({
	        type: 'POST',
	        url: _divida.prps.ajaxFile,
	        data: post,
	        dataType: 'text',
	        success: function(data){
	        	data = JSON.parse(data);
	        	$('#empresa').empty();
	        	$('#cliente').empty();
	        	$('#empresa').html(data[0].empresa);
	        	$('#cliente').html(data[0].cliente);
	        },
          error: function (jqXHR, exception) {
		        var msg = '';
		        if (jqXHR.status === 0) {
		            msg = 'Not connect.\n Verify Network.';
		        } else if (jqXHR.status == 404) {
		            msg = 'Requested page not found. [404]';
		        } else if (jqXHR.status == 500) {
		            msg = 'Internal Server Error [500].';
		        } else if (exception === 'parsererror') {
		            msg = 'Requested JSON parse failed.';
		        } else if (exception === 'timeout') {
		            msg = 'Time out error.';
		        } else if (exception === 'abort') {
		            msg = 'Ajax request aborted.';
		        } else {
		            msg = 'Uncaught Error.\n' + jqXHR.responseText;
		        }
		        console.error('erro ao obter os dados da Divida: '+msg);
		    	}
        });
	  	}
	  };

	  _divida.getParameterByName = function(name, url){
	  	if (!url) url = window.location.href;
	    url = url.toLowerCase(); // correcao em caso de case sensitive
	    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// correcao em caso de case sensitive
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	  };

	  _divida.DeleteDivida = function(){
	  	$(document).on('click','a.btn-delete',function(){
	  		let id_divida = $(this).attr('data-id-divida');
	  		let confirma = window.confirm('Deseja realmente excluir essa Dívida?');
	  		if(confirma){
	  			let post = {};
		  		post['opcao'] = 'delete-divida';
		  		post['id_divida'] = id_divida;
		  		$.ajax({
		        type: 'POST',
		        url: _divida.prps.ajaxFile,
		        data: post,
		        dataType: 'text',
		        success: function(data){
		        	if(data == 'deletou'){
		        		alert('Dívida excluída com sucesso');
		        		_divida.loadPageListarDividas();
		        	}else if(data == 'nao deletou'){
		        		alert('Não foi possível excluir a Divida');
		        	}
		        },
	          error: function (jqXHR, exception) {
			        var msg = '';
			        if (jqXHR.status === 0) {
			            msg = 'Not connect.\n Verify Network.';
			        } else if (jqXHR.status == 404) {
			            msg = 'Requested page not found. [404]';
			        } else if (jqXHR.status == 500) {
			            msg = 'Internal Server Error [500].';
			        } else if (exception === 'parsererror') {
			            msg = 'Requested JSON parse failed.';
			        } else if (exception === 'timeout') {
			            msg = 'Time out error.';
			        } else if (exception === 'abort') {
			            msg = 'Ajax request aborted.';
			        } else {
			            msg = 'Uncaught Error.\n' + jqXHR.responseText;
			        }
			        console.error('erro ao excluir a Dúvida: '+msg);
			    	}
	        });
	  		}
	  	});
	  };

		this._constructor();

	}

  $(document).ready(function () {
      new Divida();
  });

})(window.jQuery);