<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<?php include('include/head.php'); ?>
</head>
  <body>
    <?php include('include/topo-site.php'); ?>
    <div class="container">
      <div class="row you-are-here">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <span class="label label-default">Você está aqui</span>
          <ol class="breadcrumb mt5px">
            <li><a href="index.php">Home</a></li>
            <li class="active">Editar Dívida</li>
          </ol>
        </div>
      </div>
      <div class="row mt5px mb20px">
        <div class="col-xs-12 col-sm-4 col-md-4"></div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <form action="javascript:void(0);" method="POST" id="cad-cliente">
            <div class="agrupa-input">
              <span class="label label-default">Selecione o Cliente Devedor</span><br>
              <select name="cliente" id="cliente" required="required" class="form-control"></select>
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Selecione a Empresa para a qual o Cliente está em Débito</span><br>
              <select name="empresa" id="empresa" required="required" class="form-control"></select>
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Valor da Débito</span><br>
              <input type="text" name="valor_divida" id="valor_divida" placeholder="Valor do Débito" value="" class="form-control mt2px" required="required" />
            </div>
            <div class="agrupa-input">
              <span class="label label-default">Dívida ativa?</span><br>
              <select name="ativo" id="ativo" required="required" class="form-control">
                <option value="1">SIM</option>
                <option value="0">NÃO</option>
              </select>
            </div>
            <div class="agrupa-input mt20px">
              <input type="hidden" name="id_divida" id="id_divida" value="" />
              <input type="submit" name="btn-atualizar" value="Atualizar" id="btn-atualizar" class="btn btn-sm btn-success pull-right" />
            </div>
          </form>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4"></div>
      </div>
    </div>
    <script src="js/divida.js"></script>
  </body>
</html>